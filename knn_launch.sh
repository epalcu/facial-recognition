salloc -N 26 -J knn ./head/slurm_target &
sleep 1
squeue -n knn | tail -n 1 | awk '{ print $NF }' | python head/split.py
mpirun -f head/tmphosts.txt -n 26 python knn.py
killall slurm_target
rm -f head/tmphosts.txt
