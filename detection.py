from __future__ import division
import os
import cv2
import sys
from mpi4py import MPI
import numpy as np
from glob import glob
from time import time
from collections import Counter
from multiprocessing import Pool
import itertools

cascade = "cascades/haarcascades/haarcascade_frontalface_default.xml"
Init_Cascade = cv2.CascadeClassifier(cascade)

def open_images(im):
    ri = cv2.imread(im)
    gi = cv2.cvtColor(ri, cv2.COLOR_BGR2GRAY)
    return gi

def detect_faces(im):
    gi = open_images(im)
    faces = Init_Cascade.detectMultiScale(gi, scaleFactor=1.1, minNeighbors=10, minSize=(100, 100), flags=cv2.CASCADE_SCALE_IMAGE)

    return show_image(gi, faces)

def show_image(i, f):
    for (x, y, w, h) in f:
        cv2.rectangle(i, (x, y), (x+w, y+h), (0, 0,
), 2)

    print "Displaying image..\n"
    cv2.imshow("Video", cv2.resize(i, (640, 480)))

if __name__ == "__main__":
    im = detect_faces("Pics/70.jpg")

