import cv2
import sys
import time
import os
from glob import glob

cascade = "cascades/haarcascades/haarcascade_frontalface_default.xml"

Init_Cascade = cv2.CascadeClassifier(cascade)

def detect_faces(v):
    while True:
        ret, image = v.read()
	gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        faces = Init_Cascade.detectMultiScale(gray_image, scaleFactor=1.1, minNeighbors=10, minSize=(100, 100), maxSize=(150, 150), flags=cv2.CASCADE_SCALE_IMAGE)

        print("Number of detected faces: {0}".format(len(faces)))
        val = show_image(image, faces, v)
        if val == "snap":
            return image
        elif val == "quit":
            return ''

def show_image(i, f, v):
    for (x, y, w, h) in f:
        cv2.rectangle(i, (x, y), (x+w, y+h), (0, 0, 255), 2)
    
    cv2.imshow("Video", cv2.resize(i, (640, 480)))
    if cv2.waitKey(1) & 0xFF == ord(' '):
        return "snap"
    elif cv2.waitKey(1) & 0xFF == ord('q'):
        return "quit"

def num_images(images):
    return int(os.path.basename(images).split(".")[0])

if __name__ == "__main__":
    image_count = map(num_images, glob("Pics/*.jpg"))
    next_image =  max(image_count)+1
    vc = cv2.VideoCapture(0)
    im = detect_faces(vc)
    vc.release()
    cv2.destroyAllWindows()
    if len(im) > 0:
        cv2.imwrite("Pics/{0}.jpg".format(next_image), im)


