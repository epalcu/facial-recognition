from __future__ import division
import os
import cv2
import sys
import mpi4py
import numpy as np
from glob import glob
from time import time
from collections import Counter

cascade = "cascades/haarcascades/haarcascade_frontalface_default.xml"
Init_Cascade = cv2.CascadeClassifier(cascade)

def open_images(im):
    ri = cv2.imread(im)
    gi = cv2.cvtColor(ri, cv2.COLOR_BGR2GRAY)
    return gi

def detect_and_train(im):
    #print "Processing image: {0}...".format(im)
    gi = open_images(im)
    faces = Init_Cascade.detectMultiScale(gi, scaleFactor=1.1, minNeighbors=10, minSize=(100, 100), flags=cv2.CASCADE_SCALE_IMAGE)
    
    for (x, y, w, h) in faces:
        if len(faces) == 1:
	    #print faces[0], int(os.path.basename(im).split(".")[0])
            return (cv2.resize(gi[y:y+h, x:x+w], (325, 325)).astype(np.float32), int(os.path.basename(im).split(".")[0]))

def samples_f32(f):
    if f != None:
        flat = f[0].flatten().astype(np.float32)
        return flat

def responses_f32(f):
    if f != None:
        flat = f[1]
        return flat

def remove_none(r, t):
    r = [i for i in r if i != None]
    t = [j for j in t if j != None]
    return np.array(r), np.array(t)

def break_lists(f):
    res=map(responses_f32, f)
    td=map(samples_f32, f)
    res, td = remove_none(res, td)
    return res, td

def train_faces(td, res, r):
    #print "\nTraining faces..\n"
    r.train(td, cv2.ml.ROW_SAMPLE, res)
    #print "Done.\n"

if __name__ == "__main__":
    recognizer = cv2.ml.KNearest_create()
    
    fls = []
    s = []
    l = []

    start_t = time()

    fls = map(detect_and_train, glob("Pics/*.jpg"))
    l, s = break_lists(fls)

    io_t = time() - start_t

    train_t = time()

    train_faces(s, l, recognizer)

    end_t = time() - train_t

    #print "Total serial I/O time elapsed in secs: {0}".format(io_t)
    #print "Total serial training time elapsed in secs: {0}".format(end_t)
    #print "Total serial computational time elapsed in secs: {0}".format(io_t+end_t)
    print io_t
        
