import sys

workers = sys.stdin.readline()

hosts = open('head/tmphosts.txt', 'w')
hosts.write("pih0\n")

tag = workers.split('[')[0]
r = workers.partition('[')[-1].rpartition(']')[0]
r = r.split(',')
for s in r:
  if '-' in s:
    a,b = s.split('-')
    for i in range(int(a),int(b)+1):
      hosts.write("{}{}\n".format(tag, i))
  else:
    hosts.write("{}{}\n".format(tag, s))
