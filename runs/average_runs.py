from __future__ import division

files = ["serial_256.txt", "parallel_2_256.txt", "parallel_3_256.txt",  "parallel_4_256.txt",  "parallel_5_256.txt",  "parallel_10_256.txt",  "parallel_15_256.txt",  "parallel_20_256.txt",  "parallel_25_256.txt",  "parallel_30_256.txt",  "parallel_32_256.txt",  "parallel_40_256.txt",  "parallel_45_256.txt",  "parallel_50_256.txt",  "parallel_55_256.txt",  "parallel_60_256.txt",  "parallel_64_256.txt"]

serial=[]
two=[]
three=[]
four=[]
five=[]
ten=[]
fifteen=[]
twenty=[]
twenty_five=[]
thirty=[]
thirty_two=[]
fourty=[]
fourty_five=[]
fifty=[]
fifty_five=[]
sixty=[]
sixty_four=[]

for i in range(0, len(files)):
    with open(files[i]) as fname:
        lines = fname.readlines()
        for line in lines:
            if i == 0:
	    	serial.append(float(line.strip('\n')))
	    elif i == 1:
		two.append(float(line.strip('\n')))
            elif i == 2:
    		three.append(float(line.strip('\n')))
            elif i == 3:
                four.append(float(line.strip('\n')))
            elif i == 4:
                five.append(float(line.strip('\n')))
            elif i == 5:
                ten.append(float(line.strip('\n')))
            elif i == 6:
                fifteen.append(float(line.strip('\n')))
            elif i == 7:
                twenty.append(float(line.strip('\n')))
            elif i == 8:
                twenty_five.append(float(line.strip('\n')))
            elif i == 9:
                thirty.append(float(line.strip('\n')))
            elif i == 10:
                thirty_two.append(float(line.strip('\n')))
            elif i == 11:
                fourty.append(float(line.strip('\n')))
            elif i == 12:
                fourty_five.append(float(line.strip('\n')))
            elif i == 13:
                fifty.append(float(line.strip('\n')))
            elif i == 14:
                fifty_five.append(float(line.strip('\n')))
            elif i == 15:
                sixty.append(float(line.strip('\n')))
            else:
                sixty_four.append(float(line.strip('\n')))
    fname.close()

print "|---------------- 1 Node ----------------|"
print "Computation time: {0}\n".format(sum(serial)/len(serial))

print "|---------------- 2 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(two)/len(two))

print "|---------------- 3 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(three)/len(three))

print "|---------------- 4 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(four)/len(four))

print "|---------------- 5 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(five)/len(five))

print "|---------------- 10 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(ten)/len(ten))

print "|---------------- 15 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(fifteen)/len(fifteen))

print "|---------------- 20 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(twenty)/len(twenty))

print "|---------------- 25 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(twenty_five)/len(twenty_five))

print "|---------------- 30 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(thirty)/len(thirty))

print "|---------------- 32 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(thirty_two)/len(thirty_two))

print "|---------------- 40 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(fourty)/len(fourty))

print "|---------------- 45 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(fourty_five)/len(fourty_five))

print "|---------------- 50 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(fifty)/len(fifty))

print "|---------------- 55 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(fifty_five)/len(fifty_five))

print "|---------------- 60 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(sixty)/len(sixty))

print "|---------------- 64 Nodes ----------------|"
print "Computation time: {0}\n".format(sum(sixty_four)/len(sixty_four))
