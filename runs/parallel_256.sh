python ../serial.py >> serial_256.txt

srun -N 2 --mpi=pmi2 python ../parallel.py >> parallel_2_256.txt

srun -N 3 --mpi=pmi2 python ../parallel.py >> parallel_3_256.txt

srun -N 4 --mpi=pmi2 python ../parallel.py >> parallel_4_256.txt

srun -N 5 --mpi=pmi2 python ../parallel.py >> parallel_5_256.txt

srun -N 10 --mpi=pmi2 python ../parallel.py >> parallel_10_256.txt

srun -N 15 --mpi=pmi2 python ../parallel.py >> parallel_15_256.txt

srun -N 20 --mpi=pmi2 python ../parallel.py >> parallel_20_256.txt

srun -N 25 --mpi=pmi2 python ../parallel.py >> parallel_25_256.txt

srun -N 30 --mpi=pmi2 python ../parallel.py >> parallel_30_256.txt

srun -N 32 --mpi=pmi2 python ../parallel.py >> parallel_32_256.txt

srun -N 40 --mpi=pmi2 python ../parallel.py >> parallel_40_256.txt

srun -N 45 --mpi=pmi2 python ../parallel.py >> parallel_45_256.txt

srun -N 50 --mpi=pmi2 python ../parallel.py >> parallel_50_256.txt

srun -N 55 --mpi=pmi2 python ../parallel.py >> parallel_55_256.txt

srun -N 60 --mpi=pmi2 python ../parallel.py >> parallel_60_256.txt

srun -N 64 --mpi=pmi2 python ../parallel.py >> parallel_64_256.txt

