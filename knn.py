from __future__ import division
import os
import cv2
import sys
from mpi4py import MPI
import numpy as np
from glob import glob
from time import time
from collections import Counter
from multiprocessing import Pool
import itertools
import time

cascade = "cascades/haarcascades/haarcascade_frontalface_default.xml"
Init_Cascade = cv2.CascadeClassifier(cascade)

################################################
################################################
########### Training Stage Functions ###########
################################################
################################################

def open_images(im):
    ri = cv2.imread(im)
    gi = cv2.cvtColor(ri, cv2.COLOR_BGR2GRAY)
    return gi

def detect_faces(im):
    gi = open_images(im)
    faces = Init_Cascade.detectMultiScale(gi, scaleFactor=1.1, minNeighbors=10, minSize=(100, 100), maxSize=(150, 150), flags=cv2.CASCADE_SCALE_IMAGE)

    for (x, y, w, h) in faces:
        if len(faces) == 1:
            return (cv2.resize(gi[y:y+h, x:x+w], (125, 125)).astype(np.float32), int(os.path.basename(im).split(".")[0]))

def samples_f32(f):
    if f != None:
        flat = f[0].flatten().astype(np.float32)
        return flat

def responses_f32(f):
    if f != None:
        flat = f[1]
        return flat

def remove_none(r, t):
    r = [i for i in r if i != None]
    t = [j for j in t if j != None]
    return np.array(r), np.array(t)

def break_lists(f):
    res = Pool(4).map(responses_f32, f)
    td = Pool(4).map(samples_f32, f)
    res, td = remove_none(res, td)
    return res, td

def train_faces(s, l, r):
    print "\nTraining faces..\n"
    r.train(s, cv2.ml.ROW_SAMPLE, l)
    print "Done.\n"

def kill_process(r):
    if r != 0:
	sys.exit(0)

def make_np(data):
    dnp = []
    for i in data:
    	dnp = dnp+list(i)
    return np.array(dnp)

################################################
################################################
########## Predicting Stage Functions ##########
################################################
################################################

def capture_face(v):
    while True:
        ret, image = v.read()
        gi = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        face = Init_Cascade.detectMultiScale(gi, scaleFactor=1.1, minNeighbors=10, minSize=(100, 100), maxSize=(150, 150), flags=cv2.CASCADE_SCALE_IMAGE)

        print("Number of detected faces: {0}".format(len(face)))
        val = show_image(image, face, v)
        if val == "snap":
            for (x, y, w, h) in face:
                return cv2.resize(gi[y:y+h, x:x+w], (125, 125))
        elif val == "quit":
            return ''

def show_image(i, f, v):
    for (x, y, w, h) in f:
        cv2.rectangle(i, (x, y), (x+w, y+h), (0, 0, 255), 2)

    cv2.imshow("Video", cv2.resize(i, (640, 480)))
    if cv2.waitKey(1) & 0xFF == ord(' '):
        return "snap"
    elif cv2.waitKey(1) & 0xFF == ord('q'):
        return "quit"

def predict_f32(f):
    flat = f.flatten().astype(np.float32)
    return flat

def predict_face(f, r):
    pd = map(predict_f32, f)
    pd=np.array(pd).flatten()
    pd=pd.reshape((1, len(pd)))
    ret, result, neighbors, dist = r.findNearest(pd, k=5)
    print "ret: {0}\nresult: {1}\nneighbors: {2}\ndist: {3}\n".format(ret, result.ravel(), neighbors.ravel(), dist.ravel())
    fresult = [val for sublist in result.tolist() for val in sublist]
    counter = Counter(fresult)
    global pred_len
    pred_len = sum(counter.values())
    map(pred_conf, counter.keys(), counter.values())

def pred_conf(k, v):
    print "\nPredicted {0} with confidence of {1}%".format(int(k), int(v/pred_len*100))

################################################
################################################
################ Main Function #################
################################################
################################################

if __name__ == "__main__":
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    compute_node = comm.Get_size()-1
    rank = comm.Get_rank()
    recognizer = cv2.ml.KNearest_create()

    if rank == 0:
	data = glob("Pics/*.jpg")
	slices = [[] for i in range(size)]
	for i, slice in enumerate(data):
            slices[i % size].append(slice)
    else:
	data = None
	slices = None

    sd = comm.scatter(slices, root=0)

    fl = []
    samples = []
    labels = []
    s = []
    l = []

    fl = Pool(4).map(detect_faces, sd)
    labels, samples = break_lists(fl)

    comm.Barrier()
    s_data = comm.gather(list(samples), root=0)
    l_data = comm.gather(list(labels), root=0)
    kill_process(rank)

    s = make_np(s_data)
    l = make_np(l_data)
    train_faces(s, l, recognizer)

    print "Initializing facial recognition..\n"

    vc = cv2.VideoCapture(0)
    cf = capture_face(vc)
    vc.release()
    cv2.destroyAllWindows()
    if len(cf) > 0:
        predict_face(cf, recognizer)


