from __future__ import division
import os
import cv2
import sys
from mpi4py import MPI
import numpy as np
from glob import glob
from time import time
from collections import Counter
from multiprocessing import Pool
import itertools
import operator
import string

cascade = "cascades/haarcascades/haarcascade_frontalface_default.xml"
Init_Cascade = cv2.CascadeClassifier(cascade)

def open_images(im):
    ri = cv2.imread(im)
    gi = cv2.cvtColor(ri, cv2.COLOR_BGR2GRAY)
    return gi

def detect_faces(im):
    gi = open_images(im)
    faces = Init_Cascade.detectMultiScale(gi, scaleFactor=1.1, minNeighbors=10, minSize=(100, 100), flags=cv2.CASCADE_SCALE_IMAGE)
    
    for (x, y, w, h) in faces:
        if len(faces) == 1:
	    #print faces[0]
            return (cv2.resize(gi[y:y+h, x:x+w], (325, 325)).astype(np.float32), int(os.path.basename(im).split(".")[0]))

def samples_f32(f):
    if f != None:
        flat = f[0].flatten().astype(np.float32)
        return flat

def responses_f32(f):
    if f != None:
        flat = f[1]
        return flat

def remove_none(r, t):
    r = [i for i in r if i != None]
    t = [j for j in t if j != None]
    return np.array(r), np.array(t)

def break_lists(f):
    res = Pool(4).map(responses_f32, f)
    td = Pool(4).map(samples_f32, f)
    res, td = remove_none(res, td)
    return res, td

def train_faces(s, l, r):
    #print "\nTraining faces..\n"
    r.train(s, cv2.ml.ROW_SAMPLE, l)
    #print "Done.\n"

def kill_process(r):
    if r != 0:
	sys.exit(0)
'''
def make_np(data):
    dnp = []
    for i in data:
    	dnp = dnp+list(i)
        print dnp
    return np.array(dnp)
'''
def make_np(data):
    dnp = list(itertools.chain.from_iterable(data))
    #dnp = sum(data, [])
    #dnp = reduce(operator.concat, data)
    #dnp = [item for sublist in data for item in sublist]
    return np.array(dnp)

if __name__ == "__main__":
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    compute_node = comm.Get_size()-1
    rank = comm.Get_rank()
    recognizer = cv2.ml.KNearest_create()
    
    if rank == 0:
	data = glob("Pics/*.jpg")
	slices = [[] for i in range(size)]
	for i, slice in enumerate(data):
            slices[i % size].append(slice)
    else:
	data = None
	slices = None

    sd = comm.scatter(slices, root=0)
    #print "Process {0}: {1}".format(rank, sd)

    fl = []
    samples = []
    labels = []
    s = []
    l = []

    start_t = time()
    fl = Pool(4).map(detect_faces, sd)

    proc_t = time() - start_t
    #print "Process {0} finished at {1}.".format(rank, proc_t)

    labels, samples = break_lists(fl)

    comm.Barrier()
    s_data = comm.gather(list(samples), root=0)
    l_data = comm.gather(list(labels), root=0)
    io_t = time() - start_t
    kill_process(rank)

    train_t = time()

    s = make_np(s_data)
    l = make_np(l_data)
    train_faces(s, l, recognizer)

    end_t = time()-train_t

    #print "Total parallel I/O time elapsed in secs: {0}".format(io_t)
    #print "Total training time elapsed in secs: {0}".format(end_t)
    #print "Total parallel computational time elapsed in secs: {0}\n".format(io_t+end_t)
    print io_t
