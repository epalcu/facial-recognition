import sys
import os
from glob import glob

class var:
    i=1

def write_image(image):
    if var.i == 57:
	sys.exit(0)
    with open(image, "rb") as fp:
	data = fp.read()
    new_image = "Pics/" + str(int(os.path.basename(image).split(".")[0]) + 300) + ".jpg"
    with open(new_image, "wb") as fp:
	fp.write(data)
    var.i = var.i+1
    print new_image

map(write_image, glob("Pics/*.jpg"))
